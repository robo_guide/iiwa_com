# Protocol Buffers version
Since protobuf dropped Java 6 support with release 3.6.0 and Sunrise is limited
to Java 6 we are stuck at version 
[3.5.1](https://github.com/protocolbuffers/protobuf/releases/tag/v3.5.1).

# Generation
Use the supplied scripts to generate the proto files.
When building with cmake, the sources are generated on the fly.