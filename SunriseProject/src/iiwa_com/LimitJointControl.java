package iiwa_com;

import com.kuka.roboticsAPI.deviceModel.JointPosition;
import com.kuka.roboticsAPI.motionModel.controlModeModel.JointImpedanceControlMode;

public class LimitJointControl {
	private final double[] JOINT_LIMITS = { Math.toRadians(170),
			Math.toRadians(120), Math.toRadians(170), Math.toRadians(120),
			Math.toRadians(170), Math.toRadians(120), Math.toRadians(175) };
	private final double[] MOVE_STIFFNESS = { 5, 1, 1, 1, 0.5, 0.5, 0.5 };
	private final double[] LIMIT_STIFFNESS = { 100, 100, 50, 100, 50, 50, 50 };
	// Scale the stiffness in this interval
	private static final double LIMIT_DIST = Math.toRadians(20);

	private JointImpedanceControlMode controlMode;

	public LimitJointControl() {
		controlMode = new JointImpedanceControlMode(MOVE_STIFFNESS);
		controlMode.setDampingForAllJoints(1);
	}

	public JointImpedanceControlMode getControlMode() {
		return controlMode;
	}

	public JointPosition adjustPosition(JointPosition goal) {
		JointPosition result = new JointPosition(goal);
		for (int i = 0; i < result.getAxisCount(); i++) {
			double limit = JOINT_LIMITS[i] - LIMIT_DIST;
			if (goal.get(i) > limit) {
				result.set(i, limit);
			} else if (goal.get(i) < -limit) {
				result.set(i, -limit);
			}
		}
		return result;
	}

	public void adjustStiffness(JointPosition currentPosition) {
		double[] result = MOVE_STIFFNESS.clone();
		for (int i = 0; i < result.length; i++) {
			double limit = JOINT_LIMITS[i] - LIMIT_DIST;
			if (Math.abs(currentPosition.get(i)) > limit) {
				result[i] = (Math.abs(currentPosition.get(i)) - limit)
						/ LIMIT_DIST * LIMIT_STIFFNESS[i];
			}
		}
		controlMode.setStiffness(result);
	}
}
