package application;

import iiwa_com.TcpServer;

import easy_move.IiwaJointLimits;
import easy_move.IiwaKinematics;
import easy_move.JointConfiguration;
import easy_move.OptimalJointPositions;

import java.io.IOException;

import com.kuka.connectivity.motionModel.smartServo.ISmartServoRuntime;
import com.kuka.connectivity.motionModel.smartServo.SmartServo;
import com.kuka.roboticsAPI.applicationModel.RoboticsAPIApplication;
import com.kuka.roboticsAPI.deviceModel.JointPosition;
import com.kuka.roboticsAPI.deviceModel.LBR;
import com.kuka.roboticsAPI.geometricModel.Frame;
import com.kuka.roboticsAPI.geometricModel.Tool;
import com.kuka.roboticsAPI.motionModel.controlModeModel.JointImpedanceControlMode;
import com.kuka.roboticsAPI.motionModel.controlModeModel.PositionControlMode;
import com.kuka.roboticsAPI.motionModel.PTPHome;
import com.kuka.roboticsAPI.persistenceModel.processDataModel.IProcessData;
import com.kuka.roboticsAPI.uiModel.ApplicationDialogType;
import com.kuka.task.ITaskLogger;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * 
 * @author Tim Übelhör
 * 
 */
public class IiwaComApp extends RoboticsAPIApplication {

	// station setup automatically determined via Dependency Injection
	@Inject
	private LBR robot;
	@Inject
	@Named("Camera_Projector")
	private Tool tool;
	@Inject
	private ITaskLogger logger;
	@Inject
	@Named("PORT")
	private IProcessData port;
	@Inject
	@Named("MOVE")
	private IProcessData moveData;
	@Inject
	@Named("STOP")
	private IProcessData stopData;
	@Inject
	@Named("HOLD")
	private IProcessData holdData;
	@Inject
	@Named("JACOBIAN_XYZ")
	private IProcessData jacobianXyz;

	private final static String homeInfoText = "The robot moves to the home position.";
	// preferred pose parametrization. NaN indicates that this joint is free to
	// move
	private final JointConfiguration preferredConfiguration = new JointConfiguration(
			Math.toRadians(0), Math.toRadians(30), Math.toRadians(0),
			Math.toRadians(-70), Math.toRadians(0), Math.toRadians(50),
			Double.NaN);
	// kinematics of the robot + tool offset
	private IiwaKinematics kinematics = new IiwaKinematics(120);

	// impedance control parametrization
	private final double[] jointStiffness = { 2, 2, 2, 5, 2, 1, 1 };
	private JointImpedanceControlMode impedanceControl = new JointImpedanceControlMode(
			jointStiffness);
	private final double[] violationStiffnes = { 100, 100, 100, 100, 100, 100,
			100 };

	private final JointPosition homePosition = new JointPosition(
			Math.toRadians(0), Math.toRadians(30), Math.toRadians(0),
			Math.toRadians(-75), Math.toRadians(0), Math.toRadians(50),
			Math.toRadians(-10));
	private final JointPosition targetPosition = new JointPosition(
			Math.toRadians(35), Math.toRadians(55), Math.toRadians(-20),
			Math.toRadians(-50), Math.toRadians(0), Math.toRadians(50),
			Math.toRadians(45));

	// server
	private TcpServer server;

	@Override
	public void initialize() {
		// create the server
		try {
			server = new TcpServer((Integer) port.getValue(), logger, robot);
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		tool.attachTo(robot.getFlange());
		robot.setHomePosition(homePosition);
	}

	@Override
	public void run() {
		// run server
		if (server != null) {
			Thread serverThread = new Thread(server);
			serverThread.setPriority(Thread.MAX_PRIORITY);
			serverThread.start();
		}
		// go home
		PTPHome ptpHome = new PTPHome();
		ptpHome.setJointVelocityRel(0.1);
		robot.move(ptpHome);
		// validate load
		if (SmartServo.validateForImpedanceMode(tool) != true) {
			getLogger().info("Validation of Torque Model failed");
		}
		// use SmartServo for smoother experience
		if (!SmartServo.validateForImpedanceMode(tool)) {
			logger.warn("Validation of torque model failed");
		}
		// create servo runtime with 1/4 velocity
		SmartServo smartServo = new SmartServo(homePosition);
		robot.moveAsync(smartServo.setMode(impedanceControl)
				.setJointVelocityRel(0.1));
		ISmartServoRuntime servoRuntime = smartServo.getRuntime();
		while (!Thread.currentThread().isInterrupted()
				&& !(Boolean) stopData.getValue()) {
			move(servoRuntime);
			holdPosition(servoRuntime);
			easyMove(servoRuntime);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		servoRuntime.stopMotion();
	}

	/**
	 * Enables the easy handguidng
	 * 
	 * @param servoRuntime
	 */
	private void easyMove(ISmartServoRuntime servoRuntime) {
		logger.info("easy move");
		while (!(Boolean) holdData.getValue() && !(Boolean) stopData.getValue()
				&& !(Boolean) moveData.getValue()) {
			JointPosition currentPosition = robot.getCurrentJointPosition();
			JointPosition preferredPostion = preferredConfiguration
					.getPreferred(currentPosition);
			JointPosition goalPosition = OptimalJointPositions
					.calculateOptimum(currentPosition, preferredPostion,
							kinematics, (Boolean) jacobianXyz.getValue());
			servoRuntime.setDestination(goalPosition);
			preventLimits(servoRuntime);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Holds the position while moving slightly around it
	 */
	private void holdPosition(ISmartServoRuntime servoRuntime) {
		logger.info("hold position");
		impedanceControl.setStiffness(violationStiffnes);
		servoRuntime.changeControlModeSettings(impedanceControl);
		JointPosition position = robot.getCurrentJointPosition();
		// Frame meanPosition = robot.getCurrentCartesianPosition(tool
		// .getDefaultMotionFrame());
		// servoRuntime.setDestination(meanPosition);
		// Frame highPosition = new Frame(meanPosition);
		// highPosition.setZ(meanPosition.getZ() + 200);
		// Frame lowPosition = new Frame(meanPosition);
		// lowPosition.setZ(meanPosition.getZ() - 200);
		// boolean movingUp = true;
		// move up and down
		while ((Boolean) holdData.getValue() && !(Boolean) stopData.getValue()
				&& !(Boolean) moveData.getValue()) {
			// if (movingUp) {
			// servoRuntime.setDestination(highPosition);
			// movingUp = false;
			// } else {
			// servoRuntime.setDestination(lowPosition);
			// movingUp = true;
			// }
			servoRuntime.setDestination(position);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void spinUntilReached(ISmartServoRuntime servoRuntime) {
		while (!servoRuntime.isDestinationReached()
				&& !(Boolean) stopData.getValue()) {
			servoRuntime.setDestination(servoRuntime.getCurrentJointDestination());
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Holds the position while moving slightly around it
	 */
	private void move(ISmartServoRuntime servoRuntime) {
		if ((Boolean) moveData.getValue()) {
			logger.info("move");
			moveData.setValue(false);
			impedanceControl.setStiffness(violationStiffnes);
			servoRuntime.changeControlModeSettings(impedanceControl);
			servoRuntime.setDestination(homePosition);
			servoRuntime.updateWithRealtimeSystem();
			spinUntilReached(servoRuntime);
			servoRuntime.setDestination(targetPosition);
			servoRuntime.updateWithRealtimeSystem();
			spinUntilReached(servoRuntime);
			servoRuntime.setDestination(homePosition);
			servoRuntime.updateWithRealtimeSystem();
			spinUntilReached(servoRuntime);
			holdData.setValue(true);
		}
	}

	/**
	 * Adjusts the joint angles and stiffness to avoid the limits.
	 */
	private void preventLimits(ISmartServoRuntime servoRuntime) {
		double[] stiffness = IiwaJointLimits.limitStiffness(
				robot.getCurrentJointPosition(), jointStiffness,
				violationStiffnes);
		impedanceControl.setStiffness(stiffness);
		JointPosition goalPosition = servoRuntime.getCurrentJointDestination();
		goalPosition = IiwaJointLimits.limitPosition(goalPosition);
		servoRuntime.changeControlModeSettings(impedanceControl);
		servoRuntime.setDestination(goalPosition);
	}

	@Override
	public void dispose() {
		if (server != null) {
			try {
				server.close();
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
	}
}