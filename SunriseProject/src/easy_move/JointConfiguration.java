package easy_move;

import com.kuka.roboticsAPI.deviceModel.JointPosition;

/**
 * The preferred joint configuration for easy hand guiding. Accepts NaN as
 * indicator for free joint positions.
 * 
 * @author Tim �belh�r
 * 
 */
public class JointConfiguration {
	private final JointPosition preferredPosition;

	/**
	 * 
	 * @param iiwa
	 * @param preferredPosition
	 *            Use NaN to indicate free joint positions
	 */
	public JointConfiguration(JointPosition preferredPosition) {
		this.preferredPosition = preferredPosition;
	}

	public JointConfiguration(double j1, double j2, double j3,
			double j4, double j5, double j6, double j7) {
		this(new JointPosition(j1, j2, j3, j4, j5, j6, j7));
	}

	/**
	 * Replaces the NaN entries of the preferred position with the current
	 * postion of the iiwa
	 * 
	 * @param currentPosition current position of the iiwa
	 * @return the preferred position for the current robot pose
	 */
	public JointPosition getPreferred(JointPosition currentPosition) {
		JointPosition result = new JointPosition(preferredPosition);
		for (int i = 0; i < result.getAxisCount(); i++) {
			// replace NaN with the current position so it is free to move
			if (Double.isNaN(result.get(i))) {
				result.set(i, currentPosition.get(i));
			}
		}
		return result;
	}
}
