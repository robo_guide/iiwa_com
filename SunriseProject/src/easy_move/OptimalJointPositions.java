package easy_move;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.SingularValueDecomposition;

import com.kuka.roboticsAPI.deviceModel.JointPosition;

/**
 * Calculates the optimal joint configuration for the given pose and a preferred
 * joint configuration
 * 
 * @author Tim �belh�r
 * 
 */
public class OptimalJointPositions {

	/**
	 * Calculates the optimum between the preferred and the current joint
	 * position without changing the pose of the TCP.
	 * 
	 * @param currentPosition
	 *            the current joint positions of the robot
	 * @param preferredPosition
	 *            the joint position that the robot shall have
	 * @param kinematics
	 *            kinematics of the robot to get the jacobian
	 * @return a pose that is the optimum between the current and the preferred
	 *         pose
	 */
	public static JointPosition calculateOptimum(JointPosition currentPosition,
			JointPosition preferredPosition, IiwaKinematics kinematics,
			boolean jacobianXyz) {
		// convert the current and the goal position
		RealMatrix current = MatrixUtils.createColumnRealMatrix(currentPosition
				.get());
		RealMatrix preferred = MatrixUtils
				.createColumnRealMatrix(preferredPosition.get());
		// jacobian of the current pose and its pseudo inverse
		RealMatrix jacobian = kinematics.jacobian(currentPosition);
		// Use only the part for XYZ position
		if (jacobianXyz) {
			jacobian = jacobian.getSubMatrix(0, 2, 0, 6);
		}
		// SVD is good for matrices that are almost singular, QR decomposition
		// does not work!
		SingularValueDecomposition decomposed = new SingularValueDecomposition(
				jacobian);
		RealMatrix pseudoInverse = decomposed.getSolver().getInverse();
		RealMatrix identity = MatrixUtils.createRealIdentityMatrix(jacobian
				.getColumnDimension());
		// Calculate the optimum
		RealMatrix orthogonalProjector = pseudoInverse.multiply(jacobian);
		RealMatrix kernelProjector = identity.subtract(orthogonalProjector);
		RealMatrix difference = preferred.subtract(current);
		RealMatrix correction = kernelProjector.multiply(difference);
		RealMatrix optimum = current.add(correction);
		return new JointPosition(optimum.getColumn(0));
	}
}
