package easy_move;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import com.kuka.roboticsAPI.deviceModel.JointPosition;

/**
 * Kinematics of the iiwa from Denavit-Hartenberg (DH) parameters
 * 
 * @author Tim �belh�r
 * 
 */
public class IiwaKinematics {

	private final int JOINT_COUNT = 7;
	// translation along x-axis
	private final double[] a = { 0, 0, 0, 0, 0, 0, 0 };
	// translation along z-axis, without tool on flange [mm]
	private final double[] d_flange = { 360, 0, 420, 0, 400, 0, 126 };
	private final double[] d;
	// rotation around x-axis
	private final double PI_2 = Math.PI / 2;
	private final double[] alpha = { -PI_2, PI_2, PI_2, -PI_2, -PI_2, PI_2, 0 };

	// rotation around z-axis: theta is defined by the current joint positions
	/**
	 * 
	 * @param tcpOffset
	 *            distance of the tool to the robot flange
	 */
	public IiwaKinematics(double tcpOffset) {
		d = d_flange.clone();
		d[JOINT_COUNT - 1] += tcpOffset;
	}

	/**
	 * Creates the full Jacobian for the iiwa.
	 * https://arxiv.org/ftp/arxiv/papers/1707/1707.04821.pdf
	 * http://ttuadvancedrobotics.wikidot.com/the-jacobian
	 * 
	 * @param theta the current joint positions of the robot
	 * @return Jacobian of the robot configuration
	 */
	public RealMatrix jacobian(JointPosition theta) {
		// full forward transformation
		RealMatrix T_7 = forwardTransformation(theta);
		// position of the tcp
		Vector3D p_7 = new Vector3D(T_7.getColumnVector(3).getSubVector(0, 3)
				.toArray());
		// Jacobians for translation and rotation
		RealMatrix J_t = MatrixUtils.createRealMatrix(3, JOINT_COUNT);
		RealMatrix J_r = MatrixUtils.createRealMatrix(3, JOINT_COUNT);

		// Transformation to the current joint
		RealMatrix T_i = MatrixUtils.createRealIdentityMatrix(4);
		// for each column of the Jacobian
		for (int i = 0; i < JOINT_COUNT; i++) {
			Vector3D p_i = new Vector3D(T_i.getColumnVector(3)
					.getSubVector(0, 3).toArray());
			Vector3D z_i = new Vector3D(T_i.getColumnVector(2)
					.getSubVector(0, 3).toArray());
			J_t.setColumn(i, z_i.crossProduct(p_7.subtract(p_i)).toArray());
			J_r.setColumn(i, z_i.toArray());
			// update transformations
			T_i = T_i.multiply(dhTransformation(alpha[i], a[i], d[i],
					theta.get(i)));
		}

		// return the combination of both
		RealMatrix J = MatrixUtils.createRealMatrix(6, JOINT_COUNT);
		J.setSubMatrix(J_t.getData(), 0, 0);
		J.setSubMatrix(J_r.getData(), 3, 0);
		return J;
	}

	/**
	 * Calculates the forward transformation from the base to the tool
	 * 
	 * @param theta
	 * @return
	 */
	public RealMatrix forwardTransformation(JointPosition theta) {
		RealMatrix T_i = MatrixUtils.createRealIdentityMatrix(4);
		for (int i = 0; i < JOINT_COUNT; i++) {
			T_i = T_i.multiply(dhTransformation(alpha[i], a[i], d[i],
					theta.get(i)));
		}
		return T_i;
	}

	/**
	 * Calculates the DH-Transformation for one set of parameters
	 * 
	 * @param alpha
	 *            rotation around x-axis
	 * @param a
	 *            translation along x-axis
	 * @param d
	 *            translation along z-axis
	 * @param theta
	 *            rotation around z-axis
	 * @return
	 */
	private RealMatrix dhTransformation(double alpha, double a, double d,
			double theta) {
		// create matrix with all elements set to 0
		RealMatrix A = MatrixUtils.createRealMatrix(4, 4);
		A.setSubMatrix(dhRotation(alpha, theta), 0, 0);
		A.setColumn(3, dhTranslation(alpha, a, d, theta));
		return A;
	}

	/**
	 * rotation part of the transformation matrix
	 * 
	 * @param alpha
	 * @param theta
	 * @return
	 */
	private double[][] dhRotation(double alpha, double theta) {
		return new double[][] {
				{ Math.cos(theta), -Math.sin(theta) * Math.cos(alpha),
						Math.sin(theta) * Math.sin(alpha) },
				{ Math.sin(theta), Math.cos(theta) * Math.cos(alpha),
						-Math.cos(theta) * Math.sin(alpha) },
				{ 0, Math.sin(alpha), Math.cos(alpha) } };
	}

	/**
	 * translation part of the matrix, the vector has size 4 since 1 is appended
	 * for simplified insertion
	 * 
	 * @param alpha
	 * @param a
	 * @param d
	 * @param theta
	 * @return
	 */
	private double[] dhTranslation(double alpha, double a, double d,
			double theta) {
		return new double[] { a * Math.cos(theta), a * Math.sin(theta), d, 1 };
	}
}
