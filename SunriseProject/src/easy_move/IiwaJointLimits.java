package easy_move;

import com.kuka.roboticsAPI.deviceModel.JointPosition;

/**
 * Validates the joint limits and suggests joint positions and stiffnesses.
 * 
 * @author Tim �belh�r
 * 
 */
public class IiwaJointLimits {
	// KUKA LBR iiwa 820 specifications
	private static final double[] JOINT_LIMITS = { Math.toRadians(170),
			Math.toRadians(120), Math.toRadians(170), Math.toRadians(120),
			Math.toRadians(170), Math.toRadians(120), Math.toRadians(175) };
	// Scale the stiffness in this interval
	private static final double LIMIT_DIST = Math.toRadians(20);

	/**
	 * Limits the goal position to the JOINT_LIMITS - LIMIT_DIST
	 * 
	 * @param goalPosition
	 * @return
	 */
	public static JointPosition limitPosition(JointPosition goalPosition) {
		JointPosition result = new JointPosition(goalPosition);
		for (int i = 0; i < result.getAxisCount(); i++) {
			double limit = JOINT_LIMITS[i] - LIMIT_DIST;
			if (goalPosition.get(i) > limit) {
				result.set(i, limit);
			} else if (goalPosition.get(i) < -limit) {
				result.set(i, -limit);
			}
		}
		return result;
	}

	/**
	 * Limits the joints positions by projecting the difference to the limit
	 * onto the nullspace of the jacobian. Afterwards the position is
	 * hard-limited using the limitPosition method. Tests did not show any
	 * improvement over the use of limitPosition(...) method.
	 * 
	 * @param goalPosition
	 * @return
	 */
	public static JointPosition limitOptimal(JointPosition goalPosition,
			IiwaKinematics kinematics) {
		// soft limiting via optimization
		JointPosition optimized = OptimalJointPositions.calculateOptimum(
				goalPosition, limitPosition(goalPosition), kinematics, false);
		// hard limiting
		return limitPosition(optimized);
	}

	/**
	 * Checks if any joints violate their limits and adjusts the stiffness to
	 * prevent an error.
	 * 
	 * @param currentPosition
	 *            Current joint positions of the robot.
	 * @param preferredStiffness
	 *            The joint stiffness for regular operation.
	 * @param violationStiffness
	 *            The joint stiffness when a limit is violated (must be rather
	 *            high to prevent an error).
	 * @return
	 */
	public static double[] limitStiffness(JointPosition currentPosition,
			double[] preferredStiffness, double[] violationStiffness) {
		double[] result = new double[preferredStiffness.length];
		for (int i = 0; i < preferredStiffness.length; i++) {
			double limit = JOINT_LIMITS[i] - LIMIT_DIST;
			if (Math.abs(currentPosition.get(i)) > limit) {
				result[i] = (Math.abs(currentPosition.get(i)) - limit)
						/ LIMIT_DIST * violationStiffness[i];
			} else {
				result[i] = preferredStiffness[i];
			}
		}
		return result;
	}
}
