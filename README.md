# iiwa_com
Enables basic communication with a KUKA LBR iiwa robot by utilizing a Sunrise
Project and ROS. For now the package focuses on receiving the robots state.
  
# dependencies
Depends on:

- [Protocol Buffers](https://github.com/protocolbuffers/protobuf/tree/master/src)
- ROS: tf2_geometry_msgs, tf2_ros

# notes
Since the Ubuntu 18.04 repositories only contain protobuf 3.0.0 the
delimited_message_util.cc/h have been manually copied from the 3.5.1 release into this
repository. If a protobuf version >= 3.3 is released to Ubuntu these files should be
removed.